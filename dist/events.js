
define( 'events', function() {
  'use strict';

  /**
   * Events
   * @param {*} source
   * @constructor
   *
   * @typedef {string|RegExp} EventType
   * @typedef {{ }} EventOptions
   * @typedef {{ type: EventType, listener: Function, handler: Function, options: EventOptions }} EventData
   * @typedef {Array.<EventData>} EventDataArray
   *
   */
  function Events( source ) {
    this.listeners = [];
    this.source = source;
    if ( this._init ) this._init();
  }
  var pro = Events.prototype;


  // Prototype
  // ---------------------------------------------------------------------------------------------

  /**
   * Listeners Storage
   * @type {EventDataArray}
   */
  pro.listeners = [];

  /**
   * Events Source Object
   * @type {*}
   */
  pro.source = null;

  /**
   * Listen event
   * @param {EventType} type
   * @param {Function} listener
   * @param {Object} [options]
   * @returns {Events}
   */
  pro.on = function( type, listener, options ) {
    // listener must be a function
    if (!( listener instanceof Function )) return this;
    if ( !this._checkType( type )) return this;
    var
      /**
       * Hook listener function
       * @type {Function}
       */
      fn = this._proxyListener( listener ),
      /**
       * Handler data object
       * @type {EventData}
       */
      data = {
        type: type,
        listener: listener,
        handler: fn,
        options: options || {}
      };

    // handle event
    if ( this._on( data ))
      // then store handler data
      this.listeners.push( data );
    // chain
    return this;
  };

  /**
   * Remove event listener
   * @param {EventType} [type]
   * @param {Function} [listener]
   * @param {EventOptions} [options]
   * @returns {Events}
   */
  pro.off = function( type, listener, options ) {

//    // ()
//    // ( type )
//    // ( listener )
//    if ( type instanceof Function )
//      options = listeners, listener = type;
//    // ( options )

    var list = this.listeners;
    for ( var i = 0; i < list.length; i ++ ) {
      if ( this._check( list[ i ], type, listener, options ))
        if ( this._off( list[ i ] ))
          this.listeners.splice( i --, 1 );
    }
    return this;
  };

  /**
   * Emit custom event
   * (keep any arguments after the `type`}
   * @param {EventType} type
   * @returns {Events}
   */
  pro.emit = function( type ) {
    var
      // prepare arguments
      args = this._proxyArguments.apply( this, arguments ),
      // shortcut to listeners array
      list = this.listeners;
    // backward executions
    for ( var i = list.length; i > 0; i -- )
      // find handler what event exactly looks like
      if ( this._check( list[ i - 1 ], type ))
        // call handler function
        list[ i - 1 ].handler.apply( this, args );
    // chain
    return this;
  };


  // Behaviour
  // ---------------------------------------------------------------------------------------------

  pro._init = null;

  /**
   * Assign event listener
   * @param {EventData} data
   * @returns {boolean} Result flag
   * @private
   */
  pro._on = function() {
    return true;
  };

  /**
   * Detach event listener
   * @param {EventData} data
   * @returns {boolean} Result flag
   * @private
   */
  pro._off = function() {
    return true;
  };

  /**
   * Check is event `data` meet the conditions
   * @param {EventData} data
   * @param {EventType} type
   * @param {Function} listener
   * @param {EventOptions} options
   * @returns {boolean} Result flag
   * @private
   */
  pro._check = function( data, type, listener, options ) {
    // optional args
    if ( !this._checkType( type ))
      options = listener,
      listener = type,
      type = undefined;
    if (!( listener instanceof Function ))
      options = listener,
      listener = undefined;

    // listener
    if ( listener && listener !== data.listener )
      return false;
    // type
    if ( type ) {
      // type as string
      if ( 'string' == typeof data.type
        && type !== data.type )
        return false;
      // type as regexp
      if ( data.type instanceof RegExp
        && type !== data.type
        && !data.type.test( type ))
        return false;
    }
    return true;
  };

  /**
   * `type` checking
   * @param {EventType} type
   * @returns {boolean} Result flag
   * @private
   */
  pro._checkType = function( type ) {
    return 'string' == typeof type
      || type instanceof RegExp;
  };

  /**
   * Proxy given function
   * @param {Function} fn Original Function
   * @returns {Function} Proxied function
   * @private
   */
  pro._proxyListener = function( fn ) {
    return fn;
  };

  /**
   * Make changes in the given arguments to call event handler
   * @returns {Array} Result array of arguments
   * @private
   */
  pro._proxyArguments = function() {
    // remove `type` field from arguments
    return [].slice.call( arguments, 1 );
  };


  // API
  // ---------------------------------------------------------------------------------------------

  return Events;

});