
Common Events HUB
=================


Building
--------

Just do it

    npm install

It will automaticaly get any dependencies, testing source code, determinate code coverage, and generate reports.
Reports can be found in the `./coverage/` folder.

Performs (Grunt):

 * Unit testing via Jasmine2
 * Analysis of code coverage via Istanbul
 * Concatenate, minimize and gzip the source files

See `package.json`, `bower.json` and `Gruntfile.js` for more details.


Concept
-------

 * Custom event transmission with the ability to filter by types

Fully assembled this interface has several extensions:

 * Dom eventing / onReady
 * (with fallback for elder IE)
 * WebSocket
 * PostMessage

These extensions have been removed from the code because the source for demo purposes only.

Thanks.


