/*

 Emit custom event

 called only valid listeners
 type as string
 type as regexp
 listeners executed in the backward order
 listener called with correct arguments
 chained

 */


describe( 'Custom Events', function() {

    var Events;

    // Setup
    // ------------------------------------------------------------------------------------------------------------

    beforeEach( function( done ) {
        // modules
        require([ 'events' ], function() {
            Events = arguments[ 0 ];
            // callback
            done();
        });
        // mock
        jasmine.clock().install();
    });

    afterEach( function() {
        // modules
        Events = undefined;
        // mock
        jasmine.clock().uninstall();
    });


    // Specs
    // ------------------------------------------------------------------------------------------------------------

    describe( 'Events Class', function () {
        var
            events,
            obj = { test: [ 1, 2, 3 ]};

        beforeEach( function() {
            events = new Events( obj );
        });

        it( 'instance of class', function () {
            expect( events instanceof Events ).toBe( true );
        });

        it( 'corresponding public methods', function () {
            expect( 'function' == typeof events.on ).toBe( true );
            expect( 'function' == typeof events.off ).toBe( true );
            expect( 'function' == typeof events.emit ).toBe( true );
            expect( Object.prototype.toString.call( events.listeners )).toBe( '[object Array]' );
        });

        it( 'corresponding source object', function() {
            expect( events.source ).toEqual( obj );
        });

        it( 'calling of the initialization function', function() {
            var _init = Events.prototype._init,
                fn1 = Events.prototype._init = jasmine.createSpy( 'fn' );
            expect( fn1.calls.count() ).toBe( 0 );
            new Events();
            expect( fn1.calls.count() ).toBe( 1 );
            Events.prototype._init = _init;
        });
    });

    describe( 'Attach listeners (on)', function() {
        var
            events,
            fn = jasmine.createSpy( 'fn' );

        beforeEach( function() {
            events = new Events();
        });

        it( 'listener must be a function', function() {
            events.on( 'test', {} );
            expect( events.listeners.length ).toBe( 0 );
            events.on( 'test', 'function' );
            expect( events.listeners.length ).toBe( 0 );
            events.on( 'test', fn );
            expect( events.listeners.length ).toBe( 1 );
        });

        it( 'event type as string', function() {
            var len = events.listeners.length;
            events.on( 'event', fn );
            expect( events.listeners.length ).toBe( ++ len );
            events.on( 'event_name', fn );
            expect( events.listeners.length ).toBe( ++ len );
            events.on( '1event_name2', fn );
            expect( events.listeners.length ).toBe( ++ len );
        });

        it( 'event type as regexp', function() {
            var len = events.listeners.length,
                re = new RegExp( /^event\d+$/ );
            events.on( re, fn );
            expect( events.listeners.length ).toBe( ++ len );
            events.on( /^\w+\d+$/, fn );
            expect( events.listeners.length ).toBe( ++ len );
        });

        it( 'reject incorrect types', function() {
            var len = events.listeners.length;
            events.on( null, fn );
            events.on( undefined, fn );
            events.on( 12345, fn );
            events.on( Function(), fn );
            events.on( true, fn );
            expect( events.listeners.length ).toBe( len );
        });

        it( 'data object pushed to the listeners set', function() {
            var
                type1 = 'abc1',
                type2 = /^test\d+$/,
                options = { test: true, a: 1 },
                data;
            events.on( type1, fn, options );
            data = events.listeners[ events.listeners.length - 1 ];
            expect( data.type ).toBe( type1 );
            expect( data.listener ).toBe( fn );
            expect( data.handler instanceof Function ).toBeTruthy();
            expect( data.options ).toEqual( options );
        });

        it( 'listener is correctly proxied', function() {
            var data;
            events.on( 'proxied', fn );
            data = events.listeners[ events.listeners.length - 1 ];
            // just for custom events: handler === listener
            expect( data.handler ).toBe( fn );
        });

        it( 'chained', function() {
            var chain = events.on( 'chain', fn );
            expect( chain instanceof Events ).toBe( true );
        });
    });

    describe( 'Detach listeners (off)', function() {
        var events, fn;

        beforeEach( function() {
            events = new Events();
            fn = jasmine.createSpy( 'fn' );
        });

        it( 'detach all', function() {
            expect( events.listeners.length ).toBe( 0 );
            events.on( 'string', fn );
            events.on( 'other', Function() );
            events.on( /test/, fn );
            events.on( /te\w*$/, Function() );
            expect( events.listeners.length ).toBe( 4 );
            events.off();
            expect( events.listeners.length ).toBe( 0 );
        });

        describe( 'Detach events by passing type only', function() {

            it( 'passed type is string', function() {
                expect( events.listeners.length ).toBe( 0 );
                events.on( 'string', fn );
                events.on( 'string', Function() );
                events.on( 'other', Function() );
                events.on( 'next', Function() );
                expect( events.listeners.length ).toBe( 4 );
                events.off( 'string' );
                expect( events.listeners.length ).toBe( 2 );
            });

            it( 'passed type is regexp', function() {
                var re = /test/;
                expect( events.listeners.length ).toBe( 0 );
                events.on( re, fn );
                events.on( re, Function() );
                events.on( /t\s+t/, Function() );
                expect( events.listeners.length ).toBe( 3 );
                events.off( re );
                expect( events.listeners.length ).toBe( 1 );
            });

            it( 'passed type is string and event type is regexp', function() {
                expect( events.listeners.length ).toBe( 0 );
                events.on( /test/, fn );
                events.on( /te\w*$/, Function() );
                events.on( /^t\w+t/, Function() );
                events.on( /ok\w*/, Function() );
                events.on( 'next', Function() );
                expect( events.listeners.length ).toBe( 5 );
                events.off( 'test' );
                expect( events.listeners.length ).toBe( 2 );
                events.off( 'ok' );
                expect( events.listeners.length ).toBe( 1 );
            });
        });

        it( 'detach events only by their listener', function() {
            expect( events.listeners.length ).toBe( 0 );
            var fn2 = Function();
            events.on( 'string', fn );
            events.on( 'other', fn2 );
            events.on( /test/, fn );
            events.on( /other/, fn2 );
            events.on( 'next', Function() );
            expect( events.listeners.length ).toBe( 5 );
            events.off( fn );
            expect( events.listeners.length ).toBe( 3 );
            events.off( fn2 );
            expect( events.listeners.length ).toBe( 1 );
        });

        it( 'chained', function() {
            var chain;
            events.on( 'string', fn );
            chain = events.off( 'string' );
            expect( chain instanceof Events ).toBe( true );
        });
    });

    describe( 'Emit custom event', function() {
        var events, fn;

        beforeEach( function() {
            events = new Events();
            fn = jasmine.createSpy( 'fn' );
        });

        describe( 'Called only valid listeners', function() {

            it( 'type provided as string', function() {
                var fn2 = jasmine.createSpy( 'fn2' );
                expect( events.listeners.length ).toBe( 0 );
                expect( fn.calls.count() ).toBe( 0 );
                expect( fn2.calls.count() ).toBe( 0 );
                events.on( 'first', fn );
                events.on( 'first', fn2 );
                events.on( 'second', fn2 );
                events.emit( 'first' );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 1 );
                events.emit( 'second' );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 2 );
            });

            it( 'type provided as regexp', function() {
                var
                    re1 = /same/,
                    re2 = /same/,
                    re3 = /second/,
                    fn2 = jasmine.createSpy( 'fn2' ),
                    fn3 = jasmine.createSpy( 'fn3' );
                expect( events.listeners.length ).toBe( 0 );
                expect( fn.calls.count() ).toBe( 0 );
                expect( fn2.calls.count() ).toBe( 0 );
                events.on( re1, fn );
                events.on( re2, fn2 );
                events.on( re3, fn3 );
                events.emit( re1 );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 1 );
                expect( fn3.calls.count() ).toBe( 0 );
                events.emit( re2 );
                expect( fn.calls.count() ).toBe( 2 );
                expect( fn2.calls.count() ).toBe( 2 );
                expect( fn3.calls.count() ).toBe( 0 );
                events.emit( re3 );
                expect( fn.calls.count() ).toBe( 2 );
                expect( fn2.calls.count() ).toBe( 2 );
                expect( fn3.calls.count() ).toBe( 1 );
            });

            it( 'provided type is string and event type is regexp', function() {
                var
                    fn2 = jasmine.createSpy( 'fn2' ),
                    fn3 = jasmine.createSpy( 'fn3' );
                expect( events.listeners.length ).toBe( 0 );
                expect( fn.calls.count() ).toBe( 0 );
                expect( fn2.calls.count() ).toBe( 0 );
                events.on( /^test$/, fn );
                events.on( /^t\w{2}t$/, fn2 );
                events.on( /^\wes\w$/, fn3 );
                events.emit( 'test' );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 1 );
                expect( fn3.calls.count() ).toBe( 1 );
                events.emit( 'txxt' );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 2 );
                expect( fn3.calls.count() ).toBe( 1 );
                events.emit( 'xesx' );
                expect( fn.calls.count() ).toBe( 1 );
                expect( fn2.calls.count() ).toBe( 2 );
                expect( fn3.calls.count() ).toBe( 2 );
            });

            it( 'listeners executed in the backward order', function() {
                var
                    res = [],
                    fn1 = function(){ res.push( 1 ); },
                    fn2 = function(){ res.push( 2 ); },
                    fn3 = function(){ res.push( 3 ); };
                expect( events.listeners.length ).toBe( 0 );
                events.on( 'test', fn1 );
                events.on( /test/, fn2 );
                events.on( 'test', fn3 );
                events.emit( 'test' );
                expect( res ).toEqual([ 3, 2, 1 ]);
            });

            it( 'listener called with correct arguments', function() {
                expect( events.listeners.length ).toBe( 0 );
                events.on( 'test', fn );
                events.on( /^regexp$/, fn );
                events.emit( 'test', 1, { b: 'c'}, [ 'd', 3, true ], undefined );
                events.emit( 'regexp', 1, 2, 3 );
                expect( fn.calls.allArgs() ).toEqual([
                    [ 1, { b: 'c'}, [ 'd', 3, true ], undefined ],
                    [ 1, 2, 3 ]
                ])
            });

            it( 'chained', function() {
                var chain;
                events.on( 'string', fn );
                chain = events.emit( 'string' );
                expect( chain instanceof Events ).toBe( true );
            });

        });
    });




});