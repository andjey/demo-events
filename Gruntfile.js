module.exports = function( grunt ) {
	grunt.initConfig({

        // Unit Testing

		jasmine: {

			// Just Test
			test: {
				src: 'src/**/*.js',
				options: {
					specs: 'spec/*Spec.js',
					helpers: [
						'spec/*Helper.js',
						'spec/mock*.js'
					],
					template: require( 'grunt-template-jasmine-requirejs' ),
					templateOptions: {
						requireConfigFile: 'config/main.js'
					}
				}
			},

			// Code Coverage of Testing
			coverage: {
				src: [ 'src/**/*.js'],
				options: {
					specs: 'spec/*Spec.js',
					helpers: [
						'spec/*Helper.js',
						'spec/mock*.js'
					],
					template: require( 'grunt-template-jasmine-istanbul' ),
					templateOptions: {
						coverage: 'coverage/coverage.json',
						report: [
							{
								// basic html output
								type: 'html',
								options: {
									dir: 'coverage/html'
								}
							},
							{
								// cobertura - java code coverage utility
								type: 'cobertura',
								options: {
									dir: 'coverage/cobertura'
								}
							},
							{
								// output summary to the console
								type: 'text-summary'
							}
						],

						// jasmine requirejs
						// replace: false,
						template: require( 'grunt-template-jasmine-requirejs' ),
						templateOptions: {
							requireConfigFile: 'config/main.js',
//							config: {
//								instrumented: {
//									src: grunt.file.expand( 'src/**/*.js' )
//								}
//							}
						}
					}
				}
			}
		},

        // Concatenate source files

        concat: {
            dist: {
                src: [ 'src/**/*.js' ],
                dest: 'dist/events.js'
            }
        },

        // Minify sources

        uglify: {
            bower: {
                options: {
                    mangle: true,
                    compress: true
                },
                files: {
                    'dist/events.min.js': 'dist/events.js'
                }
            }
        },

        // Gzip compression

        compress: {
            main: {
                options: {
                    mode: 'gzip'
                },
                files: [
                    { expand: true, src: [ 'dist/**/*.min.js' ], ext: '.min.gz.js', filter: 'isFile' }
                ]
            }
        }

	});

	// modules
	grunt.loadNpmTasks( 'grunt-contrib-jasmine' );
    grunt.loadNpmTasks( 'grunt-contrib-concat' );
    grunt.loadNpmTasks( 'grunt-contrib-uglify' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );

	// tasks
    grunt.registerTask( 'test', [ 'jasmine:test' ]);
	grunt.registerTask( 'default', [ 'jasmine:coverage', 'concat', 'uglify', 'compress' ]);
};

